package sn.examen.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
@NamedQueries({
	@NamedQuery (name = "examen.all", query = "SELECT e FROM Examen e"),
})
public class Examen implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idExamen;
	@Column(length = 100)
	private String nomExamen;
	@OneToMany(mappedBy="examen")
	private List<Dossier> dossiers = new ArrayList<Dossier>();
	@OneToMany(mappedBy="examen")
	private List<Epreuve> epreuve = new ArrayList<Epreuve>();
	public int getIdExamen() {
		return idExamen;
	}
	public void setIdExamen(int idExamen) {
		this.idExamen = idExamen;
	}
	public String getNomExamen() {
		return nomExamen;
	}
	public void setNomExamen(String nomExamen) {
		this.nomExamen = nomExamen;
	}
	public List<Dossier> getDossiers() {
		return dossiers;
	}
	public void setDossiers(List<Dossier> dossiers) {
		this.dossiers = dossiers;
	}
	public List<Epreuve> getEpreuve() {
		return epreuve;
	}
	public void setEpreuve(List<Epreuve> epreuve) {
		this.epreuve = epreuve;
	}

}
