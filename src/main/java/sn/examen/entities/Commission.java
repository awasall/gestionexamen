package sn.examen.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
@Entity
@NamedQueries({
	@NamedQuery (name = "commission.all", query = "SELECT c FROM Commission c"),
})
public class Commission implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCommission;
	@Column(length = 100)
	private String nomCommission;
	@OneToMany(mappedBy="commission")
	private List<Enseignant> enseignant = new ArrayList<Enseignant>();
	@OneToMany(mappedBy="commission")
	private List<Epreuve> epreuve = new ArrayList<Epreuve>();
	public int getIdCommission() {
		return idCommission;
	}
	public void setIdCommission(int idCommission) {
		this.idCommission = idCommission;
	}
	public String getNomCommission() {
		return nomCommission;
	}
	public void setNomCommission(String nomCommission) {
		this.nomCommission = nomCommission;
	}
	public List<Enseignant> getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(List<Enseignant> enseignant) {
		this.enseignant = enseignant;
	}
	public List<Epreuve> getEpreuve() {
		return epreuve;
	}
	public void setEpreuve(List<Epreuve> epreuve) {
		this.epreuve = epreuve;
	}

	
	
}
