package sn.examen.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Etablissement implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEtablissement;
	@Column(length = 100)
	private String nomEtablissement;
	@Column(length = 100)
	private String code;
	@Column(length = 100)
	private String ville;
	@Column(length = 100)
	private String adresse;
	@OneToMany(mappedBy="etablissement")
	private List<Eleve> eleve = new ArrayList<Eleve>();
	@OneToMany(mappedBy="etablissement")
	private List<Enseignant> enseignant = new ArrayList<Enseignant>();
	public int getIdEtablissement() {
		return idEtablissement;
	}
	public void setIdEtablissement(int idEtablissement) {
		this.idEtablissement = idEtablissement;
	}
	public String getNomEtablissement() {
		return nomEtablissement;
	}
	public void setNomEtablissement(String nomEtablissement) {
		this.nomEtablissement = nomEtablissement;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public List<Eleve> getEleve() {
		return eleve;
	}
	public void setEleve(List<Eleve> eleve) {
		this.eleve = eleve;
	}
	public List<Enseignant> getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(List<Enseignant> enseignant) {
		this.enseignant = enseignant;
	}
	
}
