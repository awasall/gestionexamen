package sn.examen.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.DossierImpl;
import sn.examen.dao.EpreuveImpl;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.IDossier;
import sn.examen.dao.IEpreuve;
import sn.examen.dao.IEtablissement;
import sn.examen.dao.INote;
import sn.examen.dao.NoteImpl;
import sn.examen.entities.Dossier;
import sn.examen.entities.Epreuve;
import sn.examen.entities.Etablissement;
import sn.examen.entities.Note;

/**
 * Servlet implementation class LisEtablissementServlet
 */
@WebServlet("/ListeNote")
public class ListeNote extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private INote notedao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	notedao=new NoteImpl();
	}

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeNote() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Note> note=notedao.getAll();
        request.setAttribute("notes",note);
        request.getRequestDispatcher("ListeNote.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
