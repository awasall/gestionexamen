package sn.examen.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.CommisionImpl;
import sn.examen.dao.EnseignantImpl;
import sn.examen.dao.EpreuveImpl;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.ExamenImpl;
import sn.examen.dao.ICommission;
import sn.examen.dao.IEnseignant;
import sn.examen.dao.IEpreuve;
import sn.examen.dao.IEtablissement;
import sn.examen.dao.IExamen;
import sn.examen.entities.Commission;
import sn.examen.entities.Enseignant;
import sn.examen.entities.Epreuve;
import sn.examen.entities.Etablissement;
import sn.examen.entities.Examen;

/**
 * Servlet implementation class EnseignantServlet
 */
@WebServlet("/EnseignantServlet")
public class EnseignantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private IEnseignant enseignantdao;
    private IEtablissement etablissementdao;
    private ICommission commissiondao;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public EnseignantServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	enseignantdao=new EnseignantImpl();
    	commissiondao=new CommisionImpl();
    	etablissementdao=new EtablissementImpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Commission> commissions = commissiondao.getAll();
		List<Etablissement> etablissements = etablissementdao.getAll();
		request.setAttribute("commissions",commissions);
        request.setAttribute("etablissements",etablissements);
        request.getRequestDispatcher("Enseignant.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//int coef=Integer.parseInt(request.getParameter("coef"));
		String nomEnseignant=request.getParameter("nomEnseignant");
		//String matricule=request.getParameter("matricule");
		String telephone=request.getParameter("telephone");
		String adresse=request.getParameter("adresse");
		Enseignant enseignant=new Enseignant();
		Commission commission= new Commission();
		int idCommission=Integer.parseInt(request.getParameter("nomCommission"));
		commission.setIdCommission(idCommission);
		enseignant.setCommission(commission);
        
		 
		  int idEtablissement=Integer.parseInt(request.getParameter("nomEtablissement"));
		  Etablissement etablissement=new Etablissement();
		  etablissement.setIdEtablissement(idEtablissement);
           enseignant.setEtablissement(etablissement);
           enseignant.setNomEnseignant(nomEnseignant);
           String matricule=enseignantdao.genereMat();
 		  enseignant.setMatricule(matricule);
           enseignant.setMatricule(matricule);
           enseignant.setTelephone(telephone);
           enseignant.setAdresse(adresse);
			int ok= enseignantdao.add(enseignant);
			if(ok==1) {
				//request.getSession().setAttribute(password, user);
				response.sendRedirect("ListeEnseignantServlet");
			}
			else {
	            request.setAttribute("error","insertion");		}
	}
}
