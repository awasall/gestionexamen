package sn.examen.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.IEtablissement;
import sn.examen.entities.Etablissement;

/**
 * Servlet implementation class LisEtablissementServlet
 */
@WebServlet("/LisEtablissement")
public class LisEtablissementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IEtablissement etablissementdao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	etablissementdao=new EtablissementImpl();
	}

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LisEtablissementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Etablissement> etablissement=etablissementdao.getAll();
        request.setAttribute("etablissements",etablissement);
        request.getRequestDispatcher("ListeEtablissement.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
