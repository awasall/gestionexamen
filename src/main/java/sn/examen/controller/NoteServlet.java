package sn.examen.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.CommisionImpl;
import sn.examen.dao.EleveImpl;
import sn.examen.dao.EpreuveImpl;
import sn.examen.dao.ExamenImpl;
import sn.examen.dao.ICommission;
import sn.examen.dao.IEleve;
import sn.examen.dao.IEpreuve;
import sn.examen.dao.IExamen;
import sn.examen.dao.INote;
import sn.examen.dao.NoteImpl;
import sn.examen.entities.Commission;
import sn.examen.entities.Eleve;
import sn.examen.entities.Epreuve;
import sn.examen.entities.Examen;
import sn.examen.entities.Note;

/**
 * Servlet implementation class NoteServlet
 */
@WebServlet("/NoteServlet")
public class NoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private INote notedao;
    private IEleve elevedao;
    private IEpreuve epreuvedao;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public NoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	epreuvedao=new EpreuveImpl();
    	elevedao=new EleveImpl();
    	notedao=new NoteImpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Epreuve> epreuves = epreuvedao.getAll();
		List<Eleve> eleves = elevedao.getAll();
		request.setAttribute("epreuves",epreuves);
        request.setAttribute("eleves",eleves);
        request.getRequestDispatcher("Note.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int note=Integer.parseInt(request.getParameter("note"));
		//String nomEpreuve=request.getParameter("nomEpreuve");
		Note notet= new Note();
		Epreuve epreuve=new Epreuve();
		Eleve eleve= new Eleve();
		int idEleve=Integer.parseInt(request.getParameter("nomEleve"));
		eleve.setIdEleve(idEleve);
		notet.setEleve(eleve);
        
		 
		  int idEpreuve=Integer.parseInt(request.getParameter("nomEpreuve"));
		  epreuve.setIdEpreuve(idEpreuve);
		  notet.setEpreuve(epreuve);
		  notet.setNote(note);
			int ok= notedao.add(notet);
			if(ok==1) {
				//request.getSession().setAttribute(password, user);
				response.sendRedirect("ListeNote");
			}
			else {
	            request.setAttribute("error","insertion");		}
	}
}
