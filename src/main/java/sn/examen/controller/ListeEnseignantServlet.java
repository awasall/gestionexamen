package sn.examen.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.DossierImpl;
import sn.examen.dao.EnseignantImpl;
import sn.examen.dao.EpreuveImpl;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.IDossier;
import sn.examen.dao.IEnseignant;
import sn.examen.dao.IEpreuve;
import sn.examen.dao.IEtablissement;
import sn.examen.entities.Dossier;
import sn.examen.entities.Enseignant;
import sn.examen.entities.Epreuve;
import sn.examen.entities.Etablissement;

/**
 * Servlet implementation class LisEtablissementServlet
 */
@WebServlet("/ListeEnseignantServlet")
public class ListeEnseignantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IEnseignant enseignantdao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	enseignantdao=new EnseignantImpl();
	}

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeEnseignantServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Enseignant> epreuve=enseignantdao.getAll();
        request.setAttribute("enseignants",epreuve);
        request.getRequestDispatcher("ListeEnseignant.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
