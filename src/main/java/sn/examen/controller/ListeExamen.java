package sn.examen.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.ExamenImpl;
import sn.examen.dao.IEtablissement;
import sn.examen.dao.IExamen;
import sn.examen.dao.Userimpl;
import sn.examen.entities.Etablissement;
import sn.examen.entities.Examen;
import sn.examen.entities.User;
/**
 * Servlet implementation class EtablissementServlet
 */
@WebServlet("/ListeExamen")
public class ListeExamen extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private IExamen examendao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeExamen() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	examendao=new ExamenImpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Examen> examen=examendao.getAll();
        request.setAttribute("examens",examen);
        request.getRequestDispatcher("ListeExamen.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}


}
