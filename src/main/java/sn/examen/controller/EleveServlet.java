package sn.examen.controller;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sn.examen.dao.EleveImpl;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.IEleve;
import sn.examen.dao.IEtablissement;
import sn.examen.entities.Eleve;
import sn.examen.entities.Etablissement;

/**
 * Servlet implementation class EleveServlet
 */
@WebServlet("/Eleve")
public class EleveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private IEleve elevedao;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EleveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	elevedao=new EleveImpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Eleve> eleve=elevedao.getAll();
        request.setAttribute("eleves",eleve);
        request.getRequestDispatcher("Eleve.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
