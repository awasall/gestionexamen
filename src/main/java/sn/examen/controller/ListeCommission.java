package sn.examen.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.CommisionImpl;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.ExamenImpl;
import sn.examen.dao.ICommission;
import sn.examen.dao.IEtablissement;
import sn.examen.dao.IExamen;
import sn.examen.dao.Userimpl;
import sn.examen.entities.Commission;
import sn.examen.entities.Etablissement;
import sn.examen.entities.Examen;
import sn.examen.entities.User;
/**
 * Servlet implementation class EtablissementServlet
 */
@WebServlet("/ListeCommisssion")
public class ListeCommission extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private ICommission commissiondao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeCommission() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	commissiondao=new CommisionImpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Commission> commission=commissiondao.getAll();
        request.setAttribute("commissions",commission);
        request.getRequestDispatcher("ListeCommission.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}


}
