package sn.examen.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.DossierImpl;
import sn.examen.dao.EleveImpl;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.ExamenImpl;
import sn.examen.dao.IDossier;
import sn.examen.dao.IEleve;
import sn.examen.dao.IEtablissement;
import sn.examen.dao.IExamen;
import sn.examen.entities.Dossier;
import sn.examen.entities.Eleve;
import sn.examen.entities.Etablissement;
import sn.examen.entities.Examen;

/**
 * Servlet implementation class DossierServet
 */
@WebServlet(value="/DossierServet", name="dossier")
public class DossierServet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private IDossier dossierdao;
    private IEtablissement etablissementdao;
    private IExamen examendao;
    private IEleve elevedao;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public DossierServet() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	dossierdao=new DossierImpl();
    	etablissementdao=new EtablissementImpl();
    	examendao=new ExamenImpl();
    	elevedao=new EleveImpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Etablissement> etablissements = etablissementdao.getAll();
		List<Examen> examens = examendao.getAll();
		request.setAttribute("etablissements",etablissements);
        request.setAttribute("examens",examens);
        request.getRequestDispatcher("index.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomEleve=request.getParameter("nomEleve");
		String date=request.getParameter("datenaissance");
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		String nomExamen=request.getParameter("nomExamen");
		Eleve eleve=new Eleve();
        eleve.setNomEleve(nomEleve);
        Date datenaissance;
		try {
			datenaissance = f.parse(date);
	        eleve.setDatenaissance(datenaissance);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		  int idEtablissement=Integer.parseInt(request.getParameter("nomEtablissement"));
		  Etablissement etablissement= new Etablissement();
		  etablissement.setIdEtablissement(idEtablissement);
		  eleve.setEtablissement(etablissement);
		  eleve.setIdEleve(elevedao.add(eleve));
		  Dossier dossier=new Dossier();
		  String numDossier=dossierdao.genereCode();
		  dossier.setNumDossier(numDossier);
		  int idExamen=Integer.parseInt(request.getParameter("nomExamen"));
		  Examen examen=new Examen();
		  examen.setIdExamen(idExamen);
		  dossier.setExamen(examen);
		  dossier.setEleve(eleve);
		  int ok= dossierdao.add(dossier);
		     
			if(ok==1) {	
				response.sendRedirect("DetailsDossier?dossierNumero="+numDossier);
			}
			else {
	            request.setAttribute("error","Une erreur est survenue lors de la validation. Merci de réessayer SVP!");	
			}
	}

}
