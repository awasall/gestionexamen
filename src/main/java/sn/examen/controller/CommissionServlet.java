package sn.examen.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.CommisionImpl;
import sn.examen.dao.ExamenImpl;
import sn.examen.dao.ICommission;
import sn.examen.dao.IExamen;
import sn.examen.entities.Commission;
import sn.examen.entities.Examen;

/**
 * Servlet implementation class CommissionServlet
 */
@WebServlet("/Commission")
public class CommissionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private ICommission commissiondao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	commissiondao=new CommisionImpl();
	}

    public CommissionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.getRequestDispatcher("Commission.jsp").forward(request,response);	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomCommission=request.getParameter("nomCommission");
		
     Commission commission= new Commission();
     commission.setNomCommission(nomCommission);
    
		//response.getWriter().print(email);
		int ok= commissiondao.add(commission);
     
     
		if(ok==1) {
			//request.getSession().setAttribute(password, user);
			response.sendRedirect("ListeCommisssion");
		}
		else {
            request.setAttribute("error","insertion");		}
	}

}
