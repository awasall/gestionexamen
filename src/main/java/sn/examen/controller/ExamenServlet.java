package sn.examen.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.ExamenImpl;
import sn.examen.dao.IExamen;
import sn.examen.dao.IUser;
import sn.examen.dao.Userimpl;
import sn.examen.entities.Examen;
import sn.examen.entities.User;

/**
 * Servlet implementation class ExamenServlet
 */
@WebServlet("/ExamenServlet")
public class ExamenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IExamen examendao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	examendao=new ExamenImpl();
	}

    public ExamenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.getRequestDispatcher("Examen.jsp").forward(request,response);	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomExamen=request.getParameter("nomExamen");
		
     Examen examen= new Examen();
     examen.setNomExamen(nomExamen);
    
		//response.getWriter().print(email);
		int ok= examendao.add(examen);
     
     
		if(ok==1) {
			//request.getSession().setAttribute(password, user);
			response.sendRedirect("ListeExamen");
		}
		else {
            request.setAttribute("error","insertion");		}
	}

}
