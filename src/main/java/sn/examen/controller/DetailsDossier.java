package sn.examen.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.DossierImpl;
import sn.examen.dao.EleveImpl;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.ExamenImpl;
import sn.examen.dao.IDossier;
import sn.examen.entities.Dossier;

/**
 * Servlet implementation class DetailsDossier
 */
@WebServlet("/DetailsDossier")
public class DetailsDossier extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private IDossier dossierdao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailsDossier() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		dossierdao = new DossierImpl();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String dossierNumero=request.getParameter("dossierNumero");
				
		request.setAttribute("dossier",dossierdao.getByNumeroDossier(dossierNumero));
		
		request.getRequestDispatcher("DetailsDossier.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}