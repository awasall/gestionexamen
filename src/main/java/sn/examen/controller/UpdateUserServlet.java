package sn.examen.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sn.examen.entities.User;
import sn.examen.dao.IUser;
import sn.examen.dao.Userimpl;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/User")
public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IUser userdao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	userdao=new Userimpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String prenom=request.getParameter("prenom");
		String nom=request.getParameter("nom");
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		String profil=request.getParameter("profil");
     User user= new User();
     user.setPrenom(prenom);
     user.setNom(nom);
     user.setEmail(email);
     user.setPassword(password);
     user.setProfil(profil);
		//response.getWriter().print(email);
		int ok= userdao.update(user);
     
     
		if(ok==1) {
			//request.getSession().setAttribute(password, user);
			response.sendRedirect("Login");
		}
		else {
            request.setAttribute("error","insertion");		}
	}

}
