package sn.examen.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.IEtablissement;
import sn.examen.dao.Userimpl;
import sn.examen.entities.Etablissement;
import sn.examen.entities.User;
/**
 * Servlet implementation class EtablissementServlet
 */
@WebServlet("/Etablissement")
public class UEtablissementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private IEtablissement etablissementdao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UEtablissementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	etablissementdao=new EtablissementImpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.getRequestDispatcher("Etablissement.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nomEtablissement=request.getParameter("nomEtablissement");
		String code=request.getParameter("code");
		String ville=request.getParameter("ville");
		String adresse=request.getParameter("adresse");
		
     Etablissement etablissement= new Etablissement();
     etablissement.setNomEtablissement(nomEtablissement);
     etablissement.setCode(code);
     etablissement.setVille(ville);
     etablissement.setAdresse(adresse);	
		//response.getWriter().print(email);
		int ok= etablissementdao.update(etablissement);
     
		if(ok==1) {
			//request.getSession().setAttribute(password, user);
			response.sendRedirect("LisEtablissement");
		}
		else {
            request.setAttribute("error","insertion");		}
	}


}
