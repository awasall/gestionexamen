package sn.examen.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.examen.dao.CommisionImpl;
import sn.examen.dao.DossierImpl;
import sn.examen.dao.EleveImpl;
import sn.examen.dao.EpreuveImpl;
import sn.examen.dao.EtablissementImpl;
import sn.examen.dao.ExamenImpl;
import sn.examen.dao.ICommission;
import sn.examen.dao.IDossier;
import sn.examen.dao.IEleve;
import sn.examen.dao.IEpreuve;
import sn.examen.dao.IEtablissement;
import sn.examen.dao.IExamen;
import sn.examen.entities.Commission;
import sn.examen.entities.Dossier;
import sn.examen.entities.Eleve;
import sn.examen.entities.Epreuve;
import sn.examen.entities.Etablissement;
import sn.examen.entities.Examen;

/**
 * Servlet implementation class EpreuveServlet
 */
@WebServlet("/EpreuveServlet")
public class EpreuveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private IEpreuve epreuvedao;
    private ICommission commissiondao;
    private IExamen examendao;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public EpreuveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
    	epreuvedao=new EpreuveImpl();
    	commissiondao=new CommisionImpl();
    	examendao=new ExamenImpl();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Commission> commissions = commissiondao.getAll();
		List<Examen> examens = examendao.getAll();
		request.setAttribute("commissions",commissions);
        request.setAttribute("examens",examens);
        request.getRequestDispatcher("Epreuve.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int coef=Integer.parseInt(request.getParameter("coef"));
		String nomEpreuve=request.getParameter("nomEpreuve");
		
		Epreuve epreuve=new Epreuve();
		Commission commission= new Commission();
		int idCommission=Integer.parseInt(request.getParameter("nomCommission"));
		commission.setIdCommission(idCommission);
		epreuve.setCommission(commission);
        
		 
		  int idExamen=Integer.parseInt(request.getParameter("nomExamen"));
		  Examen examen=new Examen();
		  examen.setIdExamen(idExamen);
		  epreuve.setExamen(examen);
		  epreuve.setNomEpreuve(nomEpreuve);
		  epreuve.setCoef(coef);
			int ok= epreuvedao.add(epreuve);
			if(ok==1) {
				//request.getSession().setAttribute(password, user);
				response.sendRedirect("ListeEpreuve");
			}
			else {
	            request.setAttribute("error","insertion");		}
	}

}
