package sn.examen.dao;

import java.util.List;

import sn.examen.entities.Epreuve;
import sn.examen.entities.Note;

public interface INote {
	public int add(Note note);
	public int update(Note note);
	public int delete(int id);
	public Note get(int id);
	public  List<Note> getAll();
}
