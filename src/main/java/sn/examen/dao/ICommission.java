package sn.examen.dao;

import java.util.List;

import sn.examen.entities.Commission;
import sn.examen.entities.Examen;

public interface ICommission {
	public int add(Commission commission);
	public int update(Commission commission);
	public int delete(Commission commission);
	public Commission get(int id);
	public  List<Commission> getAll();
}
