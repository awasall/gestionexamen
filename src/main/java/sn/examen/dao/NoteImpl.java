package sn.examen.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sn.examen.entities.Epreuve;
import sn.examen.entities.Note;

public class NoteImpl implements INote{
	private EntityManager em;


    public NoteImpl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
        em=emf.createEntityManager();
    }
	@Override
	public int add(Note note) {
		try {
            em.getTransaction().begin();
            em.persist(note);
            em.getTransaction().commit();
            return 1;

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
	}

	@Override
	public int update(Note note) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	

	@Override
	public List<Note> getAll() {
		try {
	           return em.createQuery("SELECT N FROM Note n").getResultList();
	        }catch (Exception e){
	            e.printStackTrace();
	            return null;

	        }
	}
	


	
	@Override
	public Note get(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
