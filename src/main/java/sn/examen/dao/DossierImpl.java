package sn.examen.dao;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.catalina.User;

import sn.examen.dao.IDossier;
import sn.examen.entities.Dossier;

public class DossierImpl implements IDossier{
	private EntityManager em;


    public DossierImpl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
        em=emf.createEntityManager();
    }
	@Override
	public int add(Dossier dossier) {
		try {
            em.getTransaction().begin();
            em.persist(dossier);
            em.getTransaction().commit();
            return 1;

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
	}

	@Override
	public int update(Dossier eossier) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	

	@Override
	public List<Dossier> getAll() {
		try {
	           return em.createQuery("SELECT D FROM Dossier d").getResultList();
	        }catch (Exception e){
	            e.printStackTrace();
	            return null;

	        }
	}
	
	@Override
	public Dossier getByNumeroDossier(String numDossier) {
		try {
			return (Dossier) em.createNamedQuery("dossier.numDossier")
					.setParameter("numDossier", numDossier)
					.getSingleResult();
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String genereCode() {
		Random rn = new Random();
		  int low = 10;
	      int high = 1000;
	      int result = rn.nextInt(high-low) + low;
	      String code="000"+result;
		return code;
	}
	@Override
	public Dossier get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
