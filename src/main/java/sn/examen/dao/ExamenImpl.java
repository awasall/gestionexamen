package sn.examen.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sn.examen.entities.Examen;

public class ExamenImpl implements IExamen {

private EntityManager em;
	
	public ExamenImpl() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
		em = emf.createEntityManager();
	}
	
	@Override
	public int add(Examen examen) {
		try {
			em.getTransaction().begin();
			em.persist(examen);
			em.getTransaction().commit();
			return 1;
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int update(Examen examen) {
		try {
			em.getTransaction().begin();
			em.merge(examen);
			em.getTransaction().commit();
			return 1;
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int delete(Examen examen) {
		try {
			em.getTransaction().begin();
			em.remove(examen);
			em.getTransaction().commit();
			return 1;
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Examen get(int id) {
		return em.find(Examen.class, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Examen> getAll() {
		try {
			return em.createNamedQuery("examen.all").getResultList();
		}catch(Exception e) {
			e.printStackTrace();
			return new ArrayList<Examen>();
		}
	}
}