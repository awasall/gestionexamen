package sn.examen.dao;

import java.util.List;

import sn.examen.entities.User;

public interface IUser {
	public int add(User user);
	public int update(User roles);
	public int delete(int id);
	public User get(int id);
	public  List<User> getAll();
	public User logon(String email, String password);
    public Object count();

}
