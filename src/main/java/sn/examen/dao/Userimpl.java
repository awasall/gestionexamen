package sn.examen.dao;


import sn.examen.dao.IUser;
import sn.examen.entities.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Userimpl implements IUser {
    private EntityManager em;

    public Userimpl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
        em=emf.createEntityManager();
    }
    @Override
    public int add(User user) {
        try {
            em.getTransaction().begin();
            em.persist(user);
            em.getTransaction().commit();
            return 1;

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
    }

    @Override
    public int update(User user) {
        try {
            em.getTransaction().begin();
            em.merge(user);
            em.getTransaction().commit();
            return 1;

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
    }

    @Override
    public int delete(int id) {
        try {
            em.getTransaction().begin();
            em.remove(em.find(User.class,id));
            em.getTransaction().commit();
            return 1;

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
    }

    @Override
    public User get(int id) {
        return em.find(User.class,id);
    }

  

    @Override
    public List<User> getAll() {
        try {
           return em.createQuery("SELECT U FROM User u").getResultList();
        }catch (Exception e){
            e.printStackTrace();
            return null;

        }
    }

    @Override
	public User logon(String email, String password) {
		try {
			return (User) em.createNamedQuery("user.logon")
					.setParameter("email", email)
					.setParameter("password", password)
					.getSingleResult();
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	
       /* try {
            String sql = "SELECT U.* FROM USER AS U WHERE U.EMAIL = ? AND U.PASSWORD =?";

            Query query = em.createNativeQuery(sql, User.class);
            query.setParameter(1, email);
            query.setParameter(2, password);
            User user = (User) query.getSingleResult();
            return user;

        } catch (NoResultException e) {
            e.printStackTrace();

            return null;
        }*/
    }

    @Override
    public Object count() {

        int rowCnt= (Integer) em.createQuery("SELECT count(User.id) FROM User").getSingleResult();

    return rowCnt;


    }
}