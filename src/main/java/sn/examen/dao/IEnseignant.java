package sn.examen.dao;

import java.util.List;

import sn.examen.entities.Enseignant;
import sn.examen.entities.Epreuve;

public interface IEnseignant {
	public int add(Enseignant enseignant);
	public int update(Enseignant enseignant);
	public int delete(int id);
	public Enseignant get(int id);
	public  List<Enseignant> getAll();
	public String genereMat();
}
