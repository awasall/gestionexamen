package sn.examen.dao;

import java.util.List;

import sn.examen.entities.Etablissement;
import sn.examen.entities.Examen;
import sn.examen.entities.User;

public interface IExamen {
	public int add(Examen examen);
	public int update(Examen examen);
	public int delete(Examen examen);
	public Examen get(int id);
	public  List<Examen> getAll();
}
