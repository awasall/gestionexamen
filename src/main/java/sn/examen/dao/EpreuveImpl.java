package sn.examen.dao;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.catalina.User;

import sn.examen.dao.IDossier;
import sn.examen.entities.Dossier;
import sn.examen.entities.Epreuve;

public class EpreuveImpl implements IEpreuve{
	private EntityManager em;


    public EpreuveImpl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
        em=emf.createEntityManager();
    }
	@Override
	public int add(Epreuve epreuve) {
		try {
            em.getTransaction().begin();
            em.persist(epreuve);
            em.getTransaction().commit();
            return 1;

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
	}

	@Override
	public int update(Epreuve epreuve) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	

	@Override
	public List<Epreuve> getAll() {
		try {
	           return em.createQuery("SELECT E FROM Epreuve e").getResultList();
	        }catch (Exception e){
	            e.printStackTrace();
	            return null;

	        }
	}
	


	
	@Override
	public Epreuve get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
