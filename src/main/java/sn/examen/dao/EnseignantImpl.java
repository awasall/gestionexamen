package sn.examen.dao;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.catalina.User;

import sn.examen.dao.IDossier;
import sn.examen.entities.Dossier;
import sn.examen.entities.Enseignant;
import sn.examen.entities.Epreuve;

public class EnseignantImpl implements IEnseignant{
	private EntityManager em;


    public EnseignantImpl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
        em=emf.createEntityManager();
    }
	@Override
	public int add(Enseignant enseignant) {
		try {
            em.getTransaction().begin();
            em.persist(enseignant);
            em.getTransaction().commit();
            return 1;

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
	}

	@Override
	public int update(Enseignant enseignant) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	

	@Override
	public List<Enseignant> getAll() {
		try {
	           return em.createQuery("SELECT E FROM Enseignant e").getResultList();
	        }catch (Exception e){
	            e.printStackTrace();
	            return null;

	        }
	}
	
	@Override
	public String genereMat() {
		Random rn = new Random();
		  int low = 10;
	      int high = 1000;
	      int result = rn.nextInt(high-low) + low;
	      String code="mat"+result;
		return code;
	}

	
	@Override
	public Enseignant get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
