package sn.examen.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sn.examen.entities.Commission;
import sn.examen.entities.Examen;

public class CommisionImpl implements ICommission{
private EntityManager em;
	
	public CommisionImpl() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
		em = emf.createEntityManager();
	}
	
	@Override
	public int add(Commission commission) {
		try {
			em.getTransaction().begin();
			em.persist(commission);
			em.getTransaction().commit();
			return 1;
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int update(Commission commission) {
		try {
			em.getTransaction().begin();
			em.merge(commission);
			em.getTransaction().commit();
			return 1;
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int delete(Commission commission) {
		try {
			em.getTransaction().begin();
			em.remove(commission);
			em.getTransaction().commit();
			return 1;
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Commission get(int id) {
		return em.find(Commission.class, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Commission> getAll() {
		try {
			return em.createNamedQuery("commission.all").getResultList();
		}catch(Exception e) {
			e.printStackTrace();
			return new ArrayList<Commission>();
		}
	}

}
