package sn.examen.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import sn.examen.entities.Etablissement;
import sn.examen.entities.User;

public class EtablissementImpl implements IEtablissement{
	private EntityManager em;

    public EtablissementImpl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
        em=emf.createEntityManager();
    }
	@Override
	public int add(Etablissement etablissement) {
		try {
            em.getTransaction().begin();
            em.persist(etablissement);
            em.getTransaction().commit();
            return 1;

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
	}

	@Override
	public int update(Etablissement etablissement) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public User get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Etablissement> getAll() {
		 try {
	           return em.createQuery("SELECT E FROM Etablissement e").getResultList();
	        }catch (Exception e){
	            e.printStackTrace();
	            return null;

	        }
	}

}
