package sn.examen.dao;

import java.util.List;

import sn.examen.entities.Dossier;
import sn.examen.entities.Etablissement;
import sn.examen.entities.User;

public interface IDossier {
	public int add(Dossier dossier);
	public int update(Dossier eossier);
	public int delete(int id);
	public Dossier get(int id);
	public  List<Dossier> getAll();
	public String genereCode();
	public Dossier getByNumeroDossier(String numeroDossier);
}
