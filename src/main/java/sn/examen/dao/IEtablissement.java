package sn.examen.dao;

import java.util.List;

import sn.examen.entities.*;

public interface IEtablissement {
	public int add(Etablissement etablissement);
	public int update(Etablissement etablissement);
	public int delete(int id);
	public User get(int id);
	public  List<Etablissement> getAll();
}
