package sn.examen.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sn.examen.entities.Eleve;
import sn.examen.entities.Etablissement;
import sn.examen.entities.User;

public class EleveImpl implements IEleve{
	private EntityManager em;

    public EleveImpl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionExamen");
        em=emf.createEntityManager();
    }

	@Override
	public int add(Eleve eleve) {
		try {
            em.getTransaction().begin();
            em.persist(eleve);
            em.getTransaction().commit();
            return eleve.getIdEleve();

        }catch (Exception e){
            e.printStackTrace();
            return 0;

        }
	}

	@Override
	public int update(Eleve eleve) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Eleve get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Eleve> getAll() {
		try {
	           return em.createQuery("SELECT E FROM Eleve e").getResultList();
	        }catch (Exception e){
	            e.printStackTrace();
	            return null;

	        }
	}
}
