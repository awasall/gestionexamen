package sn.examen.filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sn.examen.entities.User;

import java.io.IOException;

//@WebFilter("/*")
public class FiltreSession implements Filter {
    public FiltreSession(){

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;
        res.getWriter().println("Execution du filtre avant la requete ...");
        HttpSession session = req.getSession();
        User user = (User)session.getAttribute("User_session");
        String chemin = req.getServletPath();
        String methode =req.getMethod();
        if (user !=null ||chemin.equals('/')||chemin.equals("/index.jsp") || chemin.equals("/login") || chemin.startsWith("/user") && methode.equals("POST")){
            chain.doFilter(request,response);
        }else{
            res.sendRedirect("Login");
        }
    }


}
