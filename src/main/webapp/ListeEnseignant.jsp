<jsp:include page="header.jsp"/>
<%@ page isELIgnored="false" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
﻿<!DOCTYPE html>
<div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Liste Des Epreuves</span>
                                    <div class="widget-buttons">
                                        <a href="#" data-toggle="maximize">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="#" data-toggle="collapse">
                                            <i class="fa fa-minus"></i>
                                        </a>
                                        <a href="#" data-toggle="dispose">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="table-toolbar">
                                        <a id="editabledatatable_new" href="EnseignantServlet" class="btn btn-default">
                                            Add New teacher
                                        </a>
                                        <div class="btn-group pull-right">
                                            <a class="btn btn-default" href="javascript:void(0);">Tools</a>
                                            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu dropdown-default">
                                                <li>
                                                    <a href="javascript:void(0);">Action</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">Another action</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">Something else here</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="javascript:void(0);">Separated link</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                            <th>
                                                    Nom Enseignant
                                                </th>
                                                <th>
                                                    Matricule
                                                </th>
                                                <th>
                                                  Telephone
                                                </th>
                                                <th>
                                                    Adresse
                                                </th>
                                                <th>
                                                    Etablissement
                                                </th>
                                                <th>
                                                    Commission
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
<c:forEach items="${enseignants}" var="e">
<tr>
<td> ${e.nomEnseignant}</td>
<td> ${e.matricule}</td>
<td> ${e.telephone}</td>
<td> ${e.adresse}</td>
<td> ${e.etablissement.nomEtablissement}</td>
<td> ${e.commission.nomCommission}</td>


<td>
 <a href="#" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
<a href="#" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>
 </td>
</tr>
</c:forEach>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
<jsp:include page="footer.jsp"/>
                    