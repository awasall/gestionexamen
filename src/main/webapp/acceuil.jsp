<jsp:include page="header.jsp"/>
<!-- Page Body -->
                <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="databox bg-white radius-bordered">
                                        <div class="databox-left bg-themesecondary">
                                            <div class="databox-piechart">
                                                <div data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt" data-percent="100" data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.1)"><span class="white font-90">50%</span></div>
                                            </div>
                                        </div>
                                        <div class="databox-right">
                                            <span class="databox-number themesecondary">28</span>
                                            <div class="databox-text darkgray">number of registrants
</div>
                                            <div class="databox-stat themesecondary radius-bordered">
                                                <i class="stat-icon icon-lg fa fa-tasks"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="databox bg-white radius-bordered">
                                        <div class="databox-left bg-themethirdcolor">
                                            <div class="databox-piechart">
                                                <div data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt" data-percent="70" data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.2)"><span class="white font-90">15%</span></div>
                                            </div>
                                        </div>
                                        <div class="databox-right">
                                            <span class="databox-number themethirdcolor">20</span>
                                            <div class="databox-text darkgray">number of admitted</div>
                                            <div class="databox-stat themethirdcolor radius-bordered">
                                                <i class="stat-icon  icon-lg fa fa-envelope-o"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="databox bg-white radius-bordered">
                                        <div class="databox-left bg-themeprimary">
                                            <div class="databox-piechart">
                                                <div id="users-pie" data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt" data-percent="15" data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.1)"><span class="white font-90">76%</span></div>
                                            </div>
                                        </div>
                                        <div class="databox-right">
                                            <span class="databox-number themeprimary">8</span>
                                            <div class="databox-text darkgray">number of failures
</div>
                                            <div class="databox-state bg-themeprimary">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    

                    
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="databox databox-xxlg databox-vertical databox-inverted">
                                <div class="databox-top bg-whitesmoke no-padding">
                                    <div class="databox-row row-2 bg-orange no-padding">
                                        <div class="databox-cell cell-1 text-align-center no-padding padding-top-5">
                                            <span class="databox-number white"><i class="fa fa-bar-chart-o no-margin"></i></span>
                                        </div>
                                        <div class="databox-cell cell-8 no-padding padding-top-5 text-align-left">
                                            <span class="databox-number white">PAGE VIEWS</span>
                                        </div>
                                        <div class="databox-cell cell-3 text-align-right padding-10">
                                            <span class="databox-text white">10 Septembre</span>
                                        </div>
                                    </div>
                                    <div class="databox-row row-4">
                                        <div class="databox-cell cell-6 no-padding padding-10 padding-left-20 text-align-left">
                                            <span class="databox-number orange no-margin">92</span>
                                            <span class="databox-text sky no-margin">OVERAL VIEWS</span>
                                        </div>
                                        <div class="databox-cell cell-2 no-padding padding-10 text-align-left">
                                            <span class="databox-number orange no-margin">10</span>
                                            <span class="databox-text darkgray no-margin">THIS WEEK</span>
                                        </div>
                                        <div class="databox-cell cell-2 no-padding padding-10 text-align-left">
                                            <span class="databox-number orange no-margin">6</span>
                                            <span class="databox-text darkgray no-margin">YESTERDAY</span>
                                        </div>
                                        <div class="databox-cell cell-2 no-padding padding-10 text-align-left">
                                            <span class="databox-number orange no-margin">60</span>
                                            <span class="databox-text darkgray no-margin">TODAY</span>
                                        </div>
                                    </div>
                                    <div class="databox-row row-6 no-padding">
                                        <div class="databox-sparkline">
                                            <span data-sparkline="line" data-height="126px" data-width="100%" data-fillcolor="#37c2e2" data-linecolor="#37c2e2"
                                                  data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#ffce55"
                                                  data-highlightspotcolor="#f5f5f5 " data-highlightlinecolor="#f5f5f5"
                                                  data-linewidth="2" data-spotradius="0">
                                                5,7,6,5,9,4,3,7,2
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="databox-bottom bg-sky no-padding">
                                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                                        <span class="databox-header white">Mon</span>
                                    </div>
                                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                                        <span class="databox-header white">Tues</span>
                                    </div>
                                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                                        <span class="databox-header white">Wed</span>
                                    </div>
                                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                                        <span class="databox-header white">Thu</span>
                                    </div>
                                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                                        <span class="databox-header white">Fri</span>
                                    </div>
                                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                                        <span class="databox-header white">Sat</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="databox databox-xxlg databox-vertical databox-shadowed bg-white radius-bordered padding-5">
                                <div class="databox-top">
                                    <div class="databox-row row-12">
                                        <div class="databox-cell cell-3 text-center">
                                            <div class="databox-number number-xxlg sonic-silver">10</div>
                                            <div class="databox-text storm-cloud">online</div>
                                        </div>
                                        <div class="databox-cell cell-9 text-align-center">
                                            <div class="databox-row row-6 text-left">
                                                <span class="badge badge-palegreen badge-empty margin-left-5"></span>
                                                <span class="databox-inlinetext uppercase darkgray margin-left-5">NEW</span>
                                                <span class="badge badge-yellow badge-empty margin-left-5"></span>
                                                <span class="databox-inlinetext uppercase darkgray margin-left-5">RETURNING</span>
                                            </div>
                                            <div class="databox-row row-6">
                                                <div class="progress bg-yellow progress-no-radius">
                                                    <div class="progress-bar progress-bar-palegreen" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 78%">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="databox-bottom">
                                    <div class="databox-row row-12">
                                        <div class="databox-cell cell-7 text-center  padding-5">
                                            <div id="dashboard-pie-chart-sources" class="chart"></div>
                                        </div>
                                        <div class="databox-cell cell-5 text-center no-padding-left padding-bottom-30">
                                            <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                <span class="databox-text sonic-silver pull-left no-margin">Type</span>
                                                <span class="databox-text sonic-silver pull-right no-margin uppercase">PCT</span>
                                            </div>
                                            <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                <span class="badge badge-blue badge-empty pull-left margin-5"></span>
                                                <span class="databox-text darkgray pull-left no-margin hidden-xs">FEED</span>
                                                <span class="databox-text darkgray pull-right no-margin uppercase">46%</span>
                                            </div>
                                            <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                <span class="badge badge-orange badge-empty pull-left margin-5"></span>
                                                <span class="databox-text darkgray pull-left no-margin hidden-xs">PREFERRAL</span>
                                                <span class="databox-text darkgray pull-right no-margin uppercase">21%</span>
                                            </div>
                                            <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                <span class="badge badge-pink badge-empty pull-left margin-5"></span>
                                                <span class="databox-text darkgray pull-left no-margin hidden-xs">DIRECT</span>
                                                <span class="databox-text darkgray pull-right no-margin uppercase">12%</span>
                                            </div>
                                            <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                <span class="badge badge-palegreen badge-empty pull-left margin-5"></span>
                                                <span class="databox-text darkgray pull-left no-margin hidden-xs">EMAIL</span>
                                                <span class="databox-text darkgray pull-right no-margin uppercase">11%</span>
                                            </div>
                                            <div class="databox-row row-2 padding-10">
                                                <span class="badge badge-yellow badge-empty pull-left margin-5"></span>
                                                <span class="databox-text darkgray pull-left no-margin hidden-xs">ORGANIC</span>
                                                <span class="databox-text darkgray pull-right no-margin uppercase">10%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /Page Body --><jsp:include page="footer.jsp"/>
