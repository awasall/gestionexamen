<%@ page isELIgnored="false" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
﻿<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 1.5.0
Purchase: https://wrapbootstrap.com/theme/beyondadmin-adminapp-angularjs-mvc-WB06R48S4
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!--Head-->
<head>
    <meta charset="utf-8" />
    <title>Detail Eleve</title>

    <meta name="description" content="register page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link id="beyond-link" href="assets/css/beyond.min.css" rel="stylesheet" />
    <link href="assets/css/demo.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet" />
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="assets/js/skins.min.js"></script>
</head>
<!--Head Ends-->
<!--Body-->
<body>
    <form action="DossierServet" method="post">
        <div class="register-container animated fadeInDown">
        <div class="registerbox bg-white">
            
            <div class="registerbox-textbox">
             <label for="numDossier" class="form-label">Numéro de dossier</label>
                <input type="text" readOnly id="numDossier" class="form-control" value="${dossier.numDossier}">
            </div>

            <div class="registerbox-textbox">
            <label for="nomEleve" class="form-label">Nom Eleve</label>
                <input type="text" readOnly id="nomEleve" class="form-control" value="${dossier.eleve.nomEleve}">
            </div>
            <div class="registerbox-textbox">
                        <label for="datenaissance" class="form-label">Date de Naissance</label>
                <input type="text" readOnly class="form-control" id="datenaissance" value="${dossier.eleve.datenaissance}" />
            </div>
            
            <div class="registerbox-textbox">
              <label for="nomEtablissement" class="form-label">Etablissement</label>
                <input type="text" readOnly class="form-control" id="etablissements" value="${dossier.eleve.etablissement.nomEtablissement}" />
            </div>
            <div class="registerbox-textbox">
             <label for="nomExamen" class="form-label">Etablissement</label>
                <input type="text" readOnly class="form-control" id="examens" value="${dossier.examen.nomExamen}" />
            </div>
           
          
           
            <div class="registerbox-submit">            
                <a href="DossierServet">Cliquer ici pour retourner</a>
            
                </div>
            
        </div>
        <div class="logobox">
        </div>
    </div>
    </form>
    <!--Basic Scripts-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="assets/js/beyond.min.js"></script>
    
</body>
<!--Body Ends-->
</html>